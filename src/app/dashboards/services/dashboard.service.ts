import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {catchError, map} from 'rxjs/operators';
import {throwError} from 'rxjs';

@Injectable()
export class DashboardService {
    private urls =  {
        'get_all_resident_users':  'https://onquestapp.herokuapp.com/user/getresidents',
        'get_all_services_lastWeek': 'https://onquestapp.herokuapp.com/user/services/getall',
        'get_all_concierges_users': 'https://onquestapp.herokuapp.com/user/getcaptains',
        'get_all_locations': 'https://onquestapp.herokuapp.com/location/getall',
    };
    httpOptions = {
        headers: new HttpHeaders({
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Headers': 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method',
            'Access-Control-Allow-Methods': 'GET,POST,OPTIONS,DELETE,PUT',
            'Allow': 'GET, POST, OPTIONS, PUT, DELETE'
        })
    };
    constructor(private httpClient: HttpClient ) {
    }
    getAllResidentUsers() {
        return this.httpClient.get<any>(this.urls.get_all_resident_users,this.httpOptions).pipe(
            map(res => res.response),
            catchError(errorRes => {
                return throwError(errorRes);
            }));
    }
    getAllLastWeekServices() {
        return this.httpClient.get<any>(this.urls.get_all_services_lastWeek, this.httpOptions).pipe(
            map(res => res),
            catchError(errorRes => {
                return throwError(errorRes);
            }));
    }
    getAllConciergesusers() {
        return this.httpClient.get<any>(this.urls.get_all_concierges_users,this.httpOptions).pipe(
            map(res => res.response),
            catchError(errorRes => {
                return throwError(errorRes);
            }));
    }
    getAllLocations() {
        return this.httpClient.get<any>(this.urls.get_all_locations,this.httpOptions).pipe(
            map(res => res.response),
            catchError(errorRes => {
                return throwError(errorRes);
            }));
    }

}
