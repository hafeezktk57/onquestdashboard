import { Routes } from '@angular/router';

import { FullComponent } from './layouts/full/full.component';
import { ComingsoonComponent } from "./comingsoon/comingsoon.component";
export const AppRoutes: Routes = [
    {
        path: '',
        component: FullComponent,
        children: [
            {
                path: '',
                redirectTo: '/dashboard',
                pathMatch: 'full'
            },
            {
                path: 'material',
                loadChildren: () => import('./material-component/material.module').then(m => m.MaterialComponentsModule)
            },
            {
                path: 'starter',
                loadChildren: () => import('./starter/starter.module').then(m => m.StarterModule)
            },
            {
            path: 'dashboard',
            loadChildren: () => import('./dashboards/dashboards.module').then(m => m.DashboardsModule)
            },
            {
              path: 'property',
              loadChildren: () => import('./properties/property.module').then(m => m.PropertyModule)
            },
            {
              path: 'residents',
              loadChildren: () => import('./resident/residents.module').then(m => m.ResidentsModule)
            },
            {
              path: 'captains',
              component: ComingsoonComponent,
              //loadChildren: () => import('./resident/residents.module').then(m => m.ResidentsModule)
            },
            {
              path: 'services',
              component: ComingsoonComponent,
              //loadChildren: () => import('./resident/residents.module').then(m => m.ResidentsModule)
            },
            {
              path: 'tasks',
              component: ComingsoonComponent,
              //loadChildren: () => import('./resident/residents.module').then(m => m.ResidentsModule)
            },
            {
              path: 'reports',
              component: ComingsoonComponent,
              //loadChildren: () => import('./resident/residents.module').then(m => m.ResidentsModule)
            },
            

            {
              path: 'calendar/:code',
              component: ComingsoonComponent,
              //loadChildren: () => import('./comingsoon/comingsoon.component').then(m => m.ComingsoonComponent)
            },
            {
              path: 'chat/:code',
              component: ComingsoonComponent,
              //loadChildren: () => import('./comingsoon/comingsoon.component').then(m => m.ComingsoonComponent)
            },
            {
              path: 'taskboard/:code',
              component: ComingsoonComponent,
              //loadChildren: () => import('./comingsoon/comingsoon.component').then(m => m.ComingsoonComponent)
            },
            {
              path: 'ticketlist/:code',
              component: ComingsoonComponent,
              //loadChildren: () => import('./comingsoon/comingsoon.component').then(m => m.ComingsoonComponent)
            },
          {
            path: 'tables',
            loadChildren: () => import('./tables/tables.module').then(m => m.TablesModule)
          }

        ]
    }
];
