import { Routes } from '@angular/router';

import {ResidenteditComponent} from "./components/edit/residentedit.component";
import {ResidentlistingComponent} from "./components/listings/residentlisting.component";

export const ResidentRoutes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: ResidentlistingComponent,
    data: {
      title: 'Resident',
      urls: [
        { title: 'Home', url: '/dashboard' },
        { title: 'Resident' }
      ]
    }
  }
/*  {
    path: ':id',
    component: ResidenteditComponent,
    data: {
      title: 'Edit Resident',
      urls: [
        { title: 'Home', url: '/dashboard' },
        { title: 'Resident', url: '/residents' },
        { title: 'Edit Resident' }
      ]
    }
  }*/
];
