import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {ResidentService} from '../../services/resident.service';
import { MatTableDataSource } from '@angular/material/table';
import { BreakpointObserver, Breakpoints, BreakpointState } from '@angular/cdk/layout';
import {MatPaginator} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";

@Component({
    selector: 'app-resident',
    templateUrl: './residentlisting.component.html',
    styleUrls: ['./residentlisting.component.css']
})

export class ResidentlistingComponent implements OnInit {
    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
    @ViewChild(MatSort, { static: true }) sort: MatSort;
    displayedColumns = ['id', 'first_name', 'property', 'unit_Number','service_day','email','phone','Action'];
    allResidentUsers = [];
    allbuilding = [];
    selectedProperty = '';
    dataSource;

  constructor(private router: Router , private residentService: ResidentService ,breakpointObserver: BreakpointObserver) {
    breakpointObserver.observe(['(max-width: 600px)']).subscribe(result => {
      this.displayedColumns = result.matches ?
        [ 'first_name', 'property', 'unit_Number','service_day','email','phone','Action'] :
        [ 'first_name', 'property', 'unit_Number','service_day','email','phone','Action'];
    });
    this.getAllResidents();
  }
    ngOnInit() {
    //  this.residentService.getAlllocations().subscribe((response: any) => {
    //      this.allbuilding = response;
    //      });
    }
    getAllResidents() {
      this.residentService.getAllResidents().subscribe((response: any) => {
          this.allResidentUsers = response;
          this.dataSource = new MatTableDataSource<any>(this.allResidentUsers);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
      });

  }
    onChange() {
        this.residentService.getAllUsers({
            'user_type': 'resident',
            'property_code': this.selectedProperty
        }).subscribe((response: any) => {
            this.allResidentUsers = response;
            this.dataSource = new MatTableDataSource<any>(this.allResidentUsers);
            this.dataSource.paginator = this.paginator;
            this.dataSource.sort = this.sort;
        });

    }
    btnClick(btntype) {
            this.router.navigate(['/residentusers/' + btntype + '/edit']);
    }
    detail(uerId) {
        this.router.navigate(['/residentusers/' + uerId + '/view']);
    }
    applyFilter(filterValue: string) {
      filterValue = filterValue.trim(); // Remove whitespace
      filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
      this.dataSource.filter = filterValue;
    }

}


