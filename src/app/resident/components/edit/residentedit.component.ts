import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import {AuthService} from '../../services/auth.service';
import {ResidentService} from '../../services/resident.service';

@Component({
    selector: 'app-location-edit',
    templateUrl: './residentedit.component.html',
    styleUrls: ['./residentedit.component.css']
})

export class ResidenteditComponent implements OnInit {

    @ViewChild('form') profileForm: NgForm;
    isError = false;
    showLoader = false;
    fileData: File = null;
    previewUrl: any = null;
    uploadedFilePath: string = null;
    user = {
        'first_name': '',
        'last_name': '',
        'profile_picture_url': '',
        'birthday': '7-11-1993',
        'street_name': '',
        'unit_number': '',
        'city': '',
        'state': '',
        'zip_code': '',
        'any_pets': '',
        'emergency_contact_first_name': '',
        'emergency_contact_last_name': '',
        'emergency_contact_phone_number': '',
        'emergency_contact_relationship': '',
        'mates': '',
        'favorite_holiday': '',
        'favorite_candy': '',
        'favorite_cookie': '',
        'favorite_beer_wine': '',
        'hobbies': '',
        'favorite_sport': '',
        'other_information': '',
        'notes': ''
    };
    user_id = null;
    alert = {};

    petProfiles = [];
    houseKeeping = [];
    rxPickup = [];
    groceryShoping = [];
    constructor(private service: AuthService, private route: ActivatedRoute , private residentService: ResidentService) {}
    ngOnInit() {
        this.user_id = this.route.snapshot.params.id;
        console.log(this.user_id);
        this.service.getUserProfile(this.user_id).subscribe((response) => {
            this.user = response;
        });
        this.residentService.getAllUserprofile(this.user_id).subscribe((response) => {
            this.petProfiles = response.pet_profile[0];
            this.houseKeeping = response.house_keeping[0];
            this.rxPickup = response.rx_pickup[0];
            this.groceryShoping = response.grocery_shoping[0];
            console.log(this.petProfiles);
        });
    }
    onSubmit () {
        this.showLoader = true;
        const data = {user_id: this.user_id, ...this.profileForm.value};
        this.service.updateUserProfile(data).subscribe((response: any) => {
            if (response) {
                this.alert = {
                    message:  'Edit successfully',
                    type: 'success'
                };
            } else {
                this.isError = true;
            }
            this.showLoader = false;
        });
    }
}
